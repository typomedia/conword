<?php

namespace ConwordBundle\Controller;

use ConwordBundle\Helper\ImageHelper;
use ConwordBundle\Service\WordService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Annotation\Route;
use Typomedia\Sysinfo\SysinfoFactory;

class StartController extends AbstractController
{
    /**
     * @var array
     */
    public $docs;

    /**
     * @Route(path="/")
     */
    public function indexAction()
    {
        $finder = new Finder();
        $finder->sortByModifiedTime();
        $finder->reverseSorting();
        $path = $this->getParameter('kernel.project_dir') . '/web';
        $finder->files()->in($path)->name('*.docx');

        return $this->render('@Conword/start/index.html.twig', [
            'files' => $finder
        ]);
    }

    /**
     * @Route(path="/about")
     */
    public function aboutAction()
    {
        $sysinfo = SysinfoFactory::create();

        $system = [
            'os' => $sysinfo->getOsRelease(),
            'cpu' => $sysinfo->getCpuModel(),
            'ram' => $sysinfo->getTotalMem(),
            'php' => $sysinfo->getPhpVersion(),
            'symfony' => Kernel::VERSION
        ];

        return $this->render('@Conword/start/about.html.twig', ['system' => $system]);
    }

    /**
     * @Route("/remove/{file}")
     */
    public function removeAction(string $file)
    {
        $fs = new Filesystem();
        $fs->remove($file);

        return $this->redirectToRoute('conword_start_index');
    }

    /**
     * @Route("/upload")
     */
    public function uploadAction(Request $request)
    {
        $files = $request->files->get('file');
        $template = $this->getParameter('application')['template'];

        foreach ($files as $file) {
            $name = $file->getClientOriginalName();
            $type = $file->getMimetype();

            if (in_array($type, ['image/jpeg', 'image/png', 'image/bmp', 'image/gif']) ) {
                $fs = new Filesystem();
                $fs->copy($file, $name);
            }
        }

        foreach ($files as $file) {
            $name = $file->getClientOriginalName();
            $type = $file->getMimetype();

            if ($type === 'text/plain') {
                $content = file_get_contents($file);
                $content = ImageHelper::removePath($content);
                $filename = basename($name, 'md') . 'docx';

                $parse = new \Parsedown();
                $content = $parse->text($content);

                $word = new WordService($template);
                $word->save($filename, $content);

                $this->docs['files'][] = $filename;
            }
        }

        return new JsonResponse($this->docs);
    }
}
