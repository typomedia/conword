<?php

namespace ConwordBundle\Helper;

class ImageHelper
{
    /**
     * @param string $content
     * @return array|string|string[]|null
     */
    public static function removePath(string $content)
    {
        $pattern = '/(!\[.*?\]\()(.+?)(\))/';

        return preg_replace_callback($pattern,
            function($matches) {
                return $matches[1] . basename($matches[2]) . $matches[3];
            }, $content
        );
    }
}