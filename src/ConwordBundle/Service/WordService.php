<?php

namespace ConwordBundle\Service;

use PhpOffice\PhpWord\Element\Image;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\SimpleType\TblWidth;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class WordService
{
    /**
     * @var string
     */
    private $template;

    public function __construct(string $template)
    {
        $this->template = $template;
    }

    /**
     * @param string $filename
     * @param string $html
     */
    public function save(string $filename, string $html)
    {
        $phpWord = new PhpWord();
        Settings::setOutputEscapingEnabled(true);
        $section = $phpWord->addSection();
        Html::addHtml($section, $html, false, false);
        $containers = $section->getElements();

        $doc = new TemplateProcessor($this->template);
        $doc->cloneBlock('htmlblock', count($containers), true, true);

        foreach ($containers as $index => $container) {
            if ($container instanceof Table) {
                $container->getStyle()->setWidth(6000);
                $container->getStyle()->setUnit(TblWidth::TWIP);
                $container->getStyle()->setBorderSize(1);
                $container->addCell(150);
            }
            if ($container instanceof TextRun) {
                foreach ($container->getElements() as $element) {
                    if ($element instanceof Image) {
                        $doc->setImageValue('html#' . ($index + 1), [
                            'path' => basename($element->getSource()),
                            'width' => 640,
                            'height' => 640,
                            'ratio' => true
                        ]);
                    }
                }
            }
            $doc->setComplexBlock('html#' . ($index + 1), $container);
        }

        $doc->saveAs($filename);
    }
}