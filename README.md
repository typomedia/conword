# Conword Markdown Converter

Conword is Markdown to Microsoft Word Converter based on [Symfony](https://symfony.com/) 4.4 LTS.

To be minimal the frontend page is build with [Bootstrap 3.3](http://getbootstrap.com/docs/3.3/) and uses [Twig](https://twig.symfony.com) as Template Engine.

## Composer

    sudo curl -LsS https://getcomposer.org/installer -o /usr/local/bin/composer
    sudo chmod a+x /usr/local/bin/composer

## Installation

    git clone https://gitlab.com/typomedia/conword.git
    cd conword
    cp app/config/param.dist app/config/param.yml
    composer install
    php -S localhost:8000 -t web

## Developer

For [PhpStorm](https://www.jetbrains.com/phpstorm/) users install the following Plugins:

* [Symfony Plugin](https://plugins.jetbrains.com/plugin/7219-symfony-plugin)

## Screenshot

![Conword Drag&Drop](screenshot.png)